<?php

/**
 * @file
 * Provides a batched rule set component to rules.
 *
 * This is based upon rules core plugin 'rule set'.
 * @See rules.plugins.inc
 */

/**
 * Implements hook_rules_plugin_info().
 */
function rules_batch_rules_plugin_info() {
  return array(
    'batch loop' => array(
      'class' => 'RulesBatchLoop',
      'embeddable' => 'RulesActionContainer',
      'extenders' => array(
        'RulesPluginUIInterface' => array(
          'class' => 'RulesBatchLoopUI',
        ),
      ),
    ),
  );
}

/**
 * Loop plugin for using batch API.
 */
class RulesBatchLoop extends RulesLoop {

  protected $itemName = 'batch loop';

  public function __construct($settings = array(), $variables = NULL) {
    $settings['batch_name'] = t('Rules Batch');
    $settings['progressive'] = FALSE;
    $settings['redirect_path'] = '';
    parent::__construct($settings, $variables);
  }

  public function evaluate(RulesState $state) {

    try {
      $batch = array(
        'operations' => array(
          array(
            'rules_batch_batch_loop_process',
            array($this, $state),
          ),
        ),
        'finished' => 'rules_batch_batch_loop_finished',
        'title' => $this->settings['batch_name'],
        'init_message' => t('Starting rules batch'),
        'progress_message' => t('Processed @current out of @total.'),
        'error_message' => t('Rules Batch has encountered an error.'),
        'progressive' => $this->settings['progressive'],
      );

      batch_set($batch);
      batch_process($this->settings['redirect_path']);
      rules_log('Batch set for %name.', array('%name' => $this->getPluginName()), RulesLog::INFO, $this);
    } catch (RulesEvaluationException $e) {
      rules_log($e->msg, $e->args, $e->severity);
      rules_log('Unable to set %name.', array('%name' => $this->getPluginName()), RulesLog::WARN, $this);
    }
  }

  public function process(RulesState $state) {
    foreach ($this->children as $action) {
      $action->evaluate($state);
    }
  }

  public function getPublicArgument($name, $info, $state) {
    return $this->getArgument($name, $info, $state);
  }

  public function label() {
    return !empty($this->label) ? $this->label : t('Batch Loop');
  }

  protected function exportSettings() {
    $export = parent::exportSettings();
    $export['BATCH_NAME'] = $this->settings['batch_name'];
    $export['PROGRESSIVE'] = $this->settings['progressive'];
    $export['REDIRECT_PATH'] = $this->settings['redirect_path'];
    return $export;
  }

  protected function importSettings($export) {
    parent::importSettings($export);
    $this->settings['batch_name'] = $export['BATCH_NAME'];
    $this->settings['progressive'] = $export['PROGRESSIVE'];
    $this->settings['redirect_path'] = $export['REDIRECT_PATH'];
  }
}

/**
 * Batch Loop plugin UI
 */
class RulesBatchLoopUI extends RulesLoopUI {

  public function form(&$form, &$form_state, $options = array(), $iterator = NULL) {
    parent::form($form, $form_state, $options);
    $settings = $this->element->settings;

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
      '#tree' => TRUE,
    );
    $form['settings']['progressive'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show the progress bar'),
      '#description' => t('Show the progress bar if you want to use this loop in the UI, otherwise if this loop is used in a scheduled rule do not show the progress bar.'),
      '#default_value' => $settings['progressive'],
    );
    $form['settings']['batch_name'] = array(
      '#type' => 'textfield',
      '#restriction' => 'input',
      '#title' => t('Batch name'),
      '#description' => t('Give this batch a name that will be displayed to the user.'),
      '#default_value' => $settings['batch_name'],
    );

    $form['settings']['redirect_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Redirect Path'),
      '#description' => t('The url to redirect to once the batch completes.'),
      '#default_value' => $settings['redirect_path'],
    );
  }

  function form_extract_values($form, &$form_state) {
    parent::form_extract_values($form, $form_state);
    $form_values = RulesPluginUI::getFormStateValues($form, $form_state);

    $this->element->settings['batch_name'] = $form_values['settings']['batch_name'];
    $this->element->settings['progressive'] = $form_values['settings']['progressive'];
    $this->element->settings['redirect_path'] = $form_values['settings']['redirect_path'];
  }
}
